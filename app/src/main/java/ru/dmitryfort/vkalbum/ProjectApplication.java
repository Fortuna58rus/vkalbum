package ru.dmitryfort.vkalbum;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

/**
 * @author Dmitry Baklanov <vk.com/fortuna58rus>
 * @since 12.01.2017
 */

public class ProjectApplication extends Application {

    private static ProjectApplication _instance;

    public static Context getContext() {
        return _instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;

        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(this);
        config.diskCacheFileCount(150)
                .memoryCacheSizePercentage(40)
                .defaultDisplayImageOptions(displayImageOptions)
                .denyCacheImageMultipleSizesInMemory()
                .threadPoolSize(6)
                .threadPriority(7)
                .imageDownloader(new BaseImageDownloader(this, 30 * 1000, 60 * 1000));
        ImageLoader.getInstance().init(config.build());
    }
}