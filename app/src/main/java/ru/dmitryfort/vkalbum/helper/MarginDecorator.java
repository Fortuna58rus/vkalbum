package ru.dmitryfort.vkalbum.helper;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * @author Dmitry Baklanov <vk.com/fortuna58rus>
 * @since 12.01.2017
 */

public class MarginDecorator extends RecyclerView.ItemDecoration {

    private int margin;

    public MarginDecorator(int margin) {
        this.margin = margin;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.top = margin;
        outRect.bottom = margin;
        outRect.left = margin;
        outRect.right = margin;
    }
}