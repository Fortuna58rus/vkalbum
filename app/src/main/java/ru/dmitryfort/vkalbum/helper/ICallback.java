package ru.dmitryfort.vkalbum.helper;

import ru.dmitryfort.vkalbum.vk.VkPhotoAlbum;

/**
 * @author Dmitry Baklanov <vk.com/fortuna58rus>
 * @since 12.01.2017
 */

public interface ICallback {
    void onSuccess(VkPhotoAlbum vkPhotoAlbum);
    void onError();
}
