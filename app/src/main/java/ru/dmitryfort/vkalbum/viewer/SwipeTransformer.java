package ru.dmitryfort.vkalbum.viewer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.widget.TextView;

import com.hannesdorfmann.swipeback.SwipeBack;
import com.hannesdorfmann.swipeback.transformer.SwipeBackTransformer;
import com.hannesdorfmann.swipeback.util.MathUtils;

import ru.dmitryfort.vkalbum.R;

/**
 * @author Dmitry Baklanov <vk.com/fortuna58rus>
 * @since 12.01.2017
 */

public class SwipeTransformer implements SwipeBackTransformer {

    private TextView textView;

    @Override
    public void onSwipeBackViewCreated(SwipeBack swipeBack, Activity activity, final View swipeBackView) {
        textView = (TextView) swipeBackView.findViewById(R.id.swipe_close_text);
        onSwipeBackReseted(swipeBack, activity);
    }

    @Override
    public void onSwipeBackCompleted(SwipeBack swipeBack, Activity activity) {
        activity.finish();
    }

    @SuppressLint("NewApi")
    @Override
    public void onSwipeBackReseted(SwipeBack swipeBack, Activity activity) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            textView.setAlpha(0);
        } else {
            // Pre Honeycomb
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onSwiping(SwipeBack swipeBack, float openRatio, int pixelOffset) {
        float startAlphaAt = 0.5f;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            textView.setAlpha(MathUtils.mapPoint(openRatio, startAlphaAt, 1f, 0f, 1f));
        } else {
            // Pre Honeycomb (Android 2.x)
        }

    }

}