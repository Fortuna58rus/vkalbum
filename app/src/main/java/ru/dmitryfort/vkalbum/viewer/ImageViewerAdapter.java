package ru.dmitryfort.vkalbum.viewer;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * @author Dmitry Baklanov <vk.com/fortuna58rus>
 * @since 12.01.2017
 */

class ImageViewerAdapter extends FragmentStatePagerAdapter {

    private final String[] images;

    public ImageViewerAdapter(FragmentManager fm, String[] images) {
        super(fm);
        this.images = images;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public ImageViewerFragment getItem(int position) {
        ImageViewerFragment fragment = new ImageViewerFragment();
        Bundle args = new Bundle();
        args.putString(ImageViewerFragment.IMAGE_URL, images[position]);
        args.putInt(ImageViewerFragment.POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(position);
    }
}

