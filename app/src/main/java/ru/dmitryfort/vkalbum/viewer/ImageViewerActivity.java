package ru.dmitryfort.vkalbum.viewer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.hannesdorfmann.swipeback.Position;
import com.hannesdorfmann.swipeback.SwipeBack;

import java.util.Locale;

import ru.dmitryfort.vkalbum.BaseActivity;
import ru.dmitryfort.vkalbum.R;

/**
 * @author Dmitry Baklanov <vk.com/fortuna58rus>
 * @since 12.01.2017
 */

public class ImageViewerActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    public static final String URLS_IMAGES = "ImageViewerActivity.URLS_IMAGES";
    public static final String CURRENT_IMAGES = "ImageViewerActivity.CURRENT_IMAGES";
    private String[] mUrls;

    @Override
    protected int getLayoutId() {
        return R.layout.viewer_activity_image;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SwipeBack.attach(this, SwipeBack.Type.OVERLAY, Position.BOTTOM, new SwipeTransformer())
                .setSwipeBackView(R.layout.viewer_swipe_back).setDividerEnabled(true);

        Intent i = getIntent();
        mUrls = i.getStringArrayExtra(URLS_IMAGES);
        int position = i.getIntExtra(CURRENT_IMAGES, 0);

        FragmentStatePagerAdapter mAdapter = new ImageViewerAdapter(getSupportFragmentManager(), mUrls);
        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewpager_viewer);
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(this);
        mViewPager.setCurrentItem(position);
        mViewPager.setOffscreenPageLimit(0);

        setTitle(String.format(Locale.getDefault(), "Фотография %d из %d", position + 1, mUrls.length));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setTitle(String.format(Locale.getDefault(), "Фотография %d из %d", position + 1, mUrls.length));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
