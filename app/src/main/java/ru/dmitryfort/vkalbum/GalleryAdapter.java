package ru.dmitryfort.vkalbum;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;
import java.util.List;

import ru.dmitryfort.vkalbum.viewer.ImageViewerActivity;
import ru.dmitryfort.vkalbum.vk.VkPhotoAlbum.VkPhotoItem;

/**
 * @author Dmitry Baklanov <vk.com/fortuna58rus>
 * @since 12.01.2017
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {

    private final Context context;
    private List<VkPhotoItem> data;
    private Integer itemWidth;

    public GalleryAdapter(Context context, List<VkPhotoItem> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.gallery_photo, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.imageView.setImageBitmap(null);
        final VkPhotoItem vkPhotoItem = data.get(position);

        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(position < 25)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .showImageOnLoading(R.drawable.placeholder)
                .displayer(new FadeInBitmapDisplayer(300, true, false, false))
                .build();

        ImageLoader.getInstance().displayImage(vkPhotoItem.getPhoto(),
                holder.imageView, displayImageOptions);

        ViewGroup.LayoutParams lp = null;

        if (itemWidth == null) {
            holder.itemView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (itemWidth == null) {
                        itemWidth = holder.itemView.getMeasuredWidth();
                        notifyDataSetChanged();
                    }
                }
            }, 50);
        } else {
            int height = vkPhotoItem.getHeight();
            int width = vkPhotoItem.getWidth();

            lp = holder.imageView.getLayoutParams();
            lp.height = itemWidth * height / width;
            lp.width = lp.height * width / height;
            holder.imageView.setLayoutParams(lp);
        }

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> urls = new ArrayList<>();
                for (VkPhotoItem item : data) {
                    urls.add(item.getPhoto());
                }

                String[] array = urls.toArray(new String[urls.size()]);

                Intent i = new Intent(context, ImageViewerActivity.class);
                i.putExtra(ImageViewerActivity.URLS_IMAGES, array);
                i.putExtra(ImageViewerActivity.CURRENT_IMAGES, position);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        };
        holder.itemView.setOnClickListener(onClickListener);
        holder.imageView.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addAll(List<VkPhotoItem> vkPhotoItems) {
        data.addAll(vkPhotoItems);
        notifyDataSetChanged();
    }

    public void update(List<VkPhotoItem> items) {
        data = new ArrayList<>(items);
        notifyDataSetChanged();
    }

    public void onConfigurationChanged() {
        itemWidth = null;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        final ImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.gallery_image);
        }
    }
}
