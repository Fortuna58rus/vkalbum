package ru.dmitryfort.vkalbum.vk;

import android.os.AsyncTask;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import ru.dmitryfort.vkalbum.helper.ICallback;

/**
 * @author Dmitry Baklanov <vk.com/fortuna58rus>
 * @since 12.01.2017
 */

public class VkPhotoAlbumAsync extends AsyncTask<VkPhotoQueryParams, Void, VkPhotoAlbum> {

    private final ICallback callback;

    public VkPhotoAlbumAsync(ICallback callback) {
        this.callback = callback;
    }

    @Override
    protected VkPhotoAlbum doInBackground(VkPhotoQueryParams... queryParams) {
        String url = "https://api.vk.com/method/photos.get?" + queryParams[0].buildParamsAsString();

        HttpRequest request = HttpRequest.get(url);
        if (request.ok()) {
            String response = request.body();

            Type vkResponseType = new TypeToken<VkResponse<VkPhotoAlbum>>() {}.getType();
            VkResponse<VkPhotoAlbum> vkResponse = new Gson().fromJson(response, vkResponseType);

            return vkResponse.getResponse();
        }

        return null;
    }

    @Override
    protected void onPostExecute(VkPhotoAlbum vkPhotoAlbum) {
        super.onPostExecute(vkPhotoAlbum);

        if (vkPhotoAlbum != null)
            callback.onSuccess(vkPhotoAlbum);
        else
            callback.onError();
    }
}
