package ru.dmitryfort.vkalbum.vk;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Dmitry Baklanov <vk.com/fortuna58rus>
 * @since 12.01.2017
 */

public class VkPhotoAlbum {
    @SerializedName("count")
    private int count;
    @SerializedName("items")
    private List<VkPhotoItem> items = null;

    public int getCount() {
        return count;
    }

    public List<VkPhotoItem> getItems() {
        return items;
    }

    public static class VkPhotoItem {
        @SerializedName("id")
        private Integer id;
        @SerializedName("album_id")
        private Integer albumId;
        @SerializedName("owner_id")
        private Integer ownerId;
        @SerializedName("user_id")
        private Integer userId;
        @SerializedName("photo_75")
        private String photo75;
        @SerializedName("photo_130")
        private String photo130;
        @SerializedName("photo_604")
        private String photo604;
        @SerializedName("photo_807")
        private String photo807;
        @SerializedName("photo_1280")
        private String photo1280;
        @SerializedName("photo_2560")
        private String photo2560;
        @SerializedName("width")
        private Integer width;
        @SerializedName("height")
        private Integer height;
        @SerializedName("text")
        private String text;
        @SerializedName("date")
        private Integer date;

        public Integer getId() {
            return id;
        }

        public Integer getAlbumId() {
            return albumId;
        }

        public Integer getOwnerId() {
            return ownerId;
        }

        public Integer getUserId() {
            return userId;
        }

        public String getPhoto75() {
            return photo75;
        }

        public String getPhoto130() {
            return photo130 == null ? getPhoto75() : photo130;
        }

        public String getPhoto604() {
            return photo604 == null ? getPhoto130() : photo604;
        }

        public String getPhoto807() {
            return photo807 == null ? getPhoto604() : photo807;
        }

        public String getPhoto1280() {
            return photo1280 == null ? getPhoto807() : photo1280;
        }

        public String getPhoto2560() {
            return photo2560 == null ? getPhoto1280() : photo2560;
        }

        public String getPhoto() {
            return getPhoto2560();
        }

        public Integer getWidth() {
            return width;
        }

        public Integer getHeight() {
            return height;
        }

        public String getText() {
            return text;
        }

        public Integer getDate() {
            return date;
        }
    }
}
