package ru.dmitryfort.vkalbum.vk;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Baklanov <vk.com/fortuna58rus>
 * @since 12.01.2017
 */

public class VkPhotoQueryParams {

    private static final String VK_VERSION = "5.62";

    private Map<String, String> params = new HashMap<>();

    public VkPhotoQueryParams ownerId(Integer id) {
        params.put("owner_id", String.valueOf(id));
        return this;
    }

    public VkPhotoQueryParams albumId(Integer id) {
        params.put("album_id", String.valueOf(id));
        return this;
    }

    public VkPhotoQueryParams offset(Integer id) {
        params.put("offset", String.valueOf(id));
        return this;
    }

    public VkPhotoQueryParams count(Integer id) {
        params.put("count", String.valueOf(id));
        return this;
    }

    public VkPhotoQueryParams unsafeParam(String param, String value) {
        params.put(param, value);
        return this;
    }

    public String buildParamsAsString() {
        params.put("v", VK_VERSION);
        params.put("rev", "1");

        String str = "";
        for (String param : params.keySet()) {
            str += param + "=" + params.get(param) + "&";
        }
        return str.substring(0, str.length() - 1);
    }
}
