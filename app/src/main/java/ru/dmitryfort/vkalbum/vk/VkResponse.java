package ru.dmitryfort.vkalbum.vk;

import com.google.gson.annotations.SerializedName;

/**
 * @author Dmitry Baklanov <vk.com/fortuna58rus>
 * @since 12.01.2017
 */

public class VkResponse<T> {

    @SerializedName("response")
    private T response;

    public T getResponse() {
        return response;
    }
}
