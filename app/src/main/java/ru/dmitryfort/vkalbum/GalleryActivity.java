package ru.dmitryfort.vkalbum;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;

import ru.dmitryfort.vkalbum.helper.ICallback;
import ru.dmitryfort.vkalbum.helper.MarginDecorator;
import ru.dmitryfort.vkalbum.helper.Utils;
import ru.dmitryfort.vkalbum.vk.VkPhotoAlbum;
import ru.dmitryfort.vkalbum.vk.VkPhotoAlbumAsync;
import ru.dmitryfort.vkalbum.vk.VkPhotoQueryParams;

/**
 * @author Dmitry Baklanov <vk.com/fortuna58rus>
 * @since 12.01.2017
 */

public class GalleryActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeLayout;
    private StaggeredGridLayoutManager mLayoutManager;
    private VkPhotoQueryParams queryParams;
    private GalleryAdapter mAdapter;
    private boolean loading;
    private int lastCount;
    private boolean isMonkey = true;
    private BroadcastReceiver mReceiver;

    @Override
    protected int getLayoutId() {
        return R.layout.gallery_activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_container);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark),
                Color.parseColor("#dedede"));

        setupRecyclerView();

        queryParams = new VkPhotoQueryParams()
                .ownerId(-64856791)
                .albumId(186142338)
                .count(40);

        if (Utils.isOnline(this)) {
            getAlbum();
        } else {
            Utils.createToast(GalleryActivity.this, R.string.desc_no_internet);

            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (Utils.isOnline(GalleryActivity.this)) {
                        if (mAdapter == null)
                            getAlbum();

                        if (mReceiver != null) {
                            unregisterReceiver(mReceiver);
                            mReceiver = null;
                        }
                    }
                }
            };

            IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
            registerReceiver(mReceiver, intentFilter);
        }
    }

    private void getAlbum() {
        ICallback callback = new ICallback() {
            @Override
            public void onSuccess(VkPhotoAlbum vkPhotoAlbum) {
                mAdapter = new GalleryAdapter(GalleryActivity.this, vkPhotoAlbum.getItems());
                mRecyclerView.setAdapter(mAdapter);
                mSwipeLayout.setRefreshing(false);
            }

            @Override
            public void onError() {
                Utils.createToast(GalleryActivity.this, R.string.desc_internet_error);
            }
        };

        requestPhotoAlbum(callback);
        mSwipeLayout.setRefreshing(true);
    }

    private void setupRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_gallery);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new StaggeredGridLayoutManager(2, OrientationHelper.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new MarginDecorator(Utils.convertDpToPx(this, 3)));
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                // lastCount - переменная для отслеживания изменения количества фотографий,
                // если кол-во фотографий с последнего запроса не изменилось, то данных больше нет
                if (lastCount == mLayoutManager.getItemCount() || loading)
                    return;

                int totalItemCount = mLayoutManager.getItemCount();
                int[] lastVisibleItem = mLayoutManager.findLastVisibleItemPositions(null);

                boolean reqUploading = false;

                // Требуется ли подгрузить ещё данных
                for (Integer item : lastVisibleItem) {
                    if (totalItemCount - item < 20) {
                        reqUploading = true;
                        break;
                    }
                }

                if (reqUploading)
                    moreUploading();
            }
        });
    }

    private void requestPhotoAlbum(ICallback callback) {
        VkPhotoAlbumAsync async = new VkPhotoAlbumAsync(callback);
        async.execute(queryParams);
    }

    private void moreUploading() {
        if (!Utils.isOnline(GalleryActivity.this)) {
            Utils.createToast(GalleryActivity.this, R.string.desc_no_internet);
            return;
        }

        loading = true;

        queryParams.offset(mAdapter.getItemCount());

        ICallback callback = new ICallback() {
            @Override
            public void onSuccess(VkPhotoAlbum vkPhotoAlbum) {
                // lastCount - переменная для отслеживания изменения количества фотографий,
                // если кол-во фотографий с последнего запроса не изменилось, то данных больше нет.
                // Запись в счетчик только после успешного запроса.
                lastCount = mAdapter.getItemCount();

                mAdapter.addAll(vkPhotoAlbum.getItems());

                mSwipeLayout.setRefreshing(false);
                loading = false;
            }

            @Override
            public void onError() {
                Utils.createToast(GalleryActivity.this, R.string.desc_internet_error);
            }
        };
        requestPhotoAlbum(callback);
    }

    @Override
    public void onRefresh() {
        if (!Utils.isOnline(this)) {
            Utils.createToast(GalleryActivity.this, R.string.desc_no_internet);
            mSwipeLayout.setRefreshing(false);
            return;
        }

        loading = true;
        lastCount = 0; //иначе будет баг с подгрузкой при lastCount == 40

        queryParams.offset(0);

        ICallback callback = new ICallback() {
            @Override
            public void onSuccess(VkPhotoAlbum vkPhotoAlbum) {
                mAdapter.update(vkPhotoAlbum.getItems());

                loading = false;

                mRecyclerView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mRecyclerView.smoothScrollToPosition(0);
                        mSwipeLayout.setRefreshing(false);
                    }
                }, 100);
            }

            @Override
            public void onError() {
                Utils.createToast(GalleryActivity.this, R.string.desc_internet_error);
            }
        };

        requestPhotoAlbum(callback);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mAdapter.onConfigurationChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_change_album:
                if (!Utils.isOnline(GalleryActivity.this)) {
                    Utils.createToast(GalleryActivity.this, R.string.desc_no_internet);
                    return false;
                }

                mSwipeLayout.setRefreshing(true);
                if (isMonkey) {
                    item.setIcon(R.drawable.ic_monkey);
                    queryParams.ownerId(-28627911).albumId(239554275);
                } else {
                    item.setIcon(R.drawable.ic_girls);
                    queryParams.ownerId(-64856791).albumId(186142338);
                }
                isMonkey = !isMonkey;
                onRefresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAdapter != null)
            mAdapter.onConfigurationChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
            mReceiver = null;
        }
    }
}
